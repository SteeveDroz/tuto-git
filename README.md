# Tutoriel git

Ce tutoriel est décomposé en plusieurs phases que voici :

## Création et récupération de projets

- [Installation et mise en route de git sur un poste Ubuntu](install.md)
- [Création d'un nouveau projet git depuis zéro](new-project.md)
- [Mise en place de git sur un projet déjà existant](existing-project.md)
- [Récupération d'un projet déjà sauvegardé](clone.md)

## Utilisation standard

- à suivre