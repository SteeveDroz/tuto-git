# Projet existant

Pour sauvegarder un projet existant avec Git, il faut procéder en deux étapes :
1. Créer le dépôt distant.
2. Créer le dépôt local et le lier au dépôt distant.

## Créer le dépôt distant

Pour créer un dépôt, procédez de manière standard, comme vu [ici](new-project.md#Création du projet sur GitLab).

## Créer le dépôt local

1. Sur le bureau ou dans un dossier, effectuer un clic droit et choisir *Git GUI*.
2. Choisir l'option *Créer un nouveau dépôt*.
3. Naviguer vers le répertoire existant et valider.
4. La fenêtre Git GUI s'ouvre, la laisser ouverte pour la suite.

## Lier le dépôt local au dépôt distant

1. Se rendre sur la page principale du projet GitLab.
2. Dans la barre située en-dessous du texte, choisir *HTTPS* et copier le lien dans la zone texte. Ce texte a la forme suivante : *https://gitlab.com/VotrePseudo/votre-depot.git*.
3. Dans la fenêtre Git GUI, choisir le menu *Dépôt distant → Ajouter*.
  1. Le nom du dépôt est par convention *origin*.
  2. L'adresse est celle copiée ci-dessus avec votre nom d'utilisateur intercalé comme ceci : *https://**VotrePseudo@**gitlab.com/VotrePseudo/votre-depot.git* . Cette action n'est pas obligatoire, mais Git vous demandera votre nom d'utilisateur à chaque connexion si elle est omise.
4. Sous *Action supplémentaire*, choisir *Ne rien faire d'autre maintenant* et cliquer sur *Ajouter*.