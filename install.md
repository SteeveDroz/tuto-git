# Installation de git

[< Retour](README.md)

Les points marqués d'une étoile (*) peuvent être remplacés par l'unique commande en fin de document.

(*) L'installation de git se fait en ligne de commande : `sudo apt-get install git git-gui` ou en cliquant sur [ce lien](apt:git,git-gui).

(*) À la suite de l'installation, installez également Nautilus Actions grâce à la commande `sudo apt-get install nautilus-actions` ou en cliquant sur [ce lien](apt:nautilus-actions).

Ouvrez Nautilus Actions avec la commande `nautilus-actions-config-tool` et ajoutez une action correspondant aux indications suivantes :
- Onglet Action :
  - Afficher l'élément dans le menu contextuel de la sélection
  - Afficher l'élément dans le menu contextuel de l'emplacement
  - Étiquette du contexte : Git GUI
- Onglet Commande :
  - Chemin : /opt/git-gui-script.sh
  - Paramètre %f
  - Répertoire de travail : %d

Il est en outre conseillé de décocher la case "Créer un menu racine Nautilus-Actions" dans le menu "Édition -> Préférences".

(*) Créez le fichier /opt/git-gui-script.sh avec votre éditeur favori et ajoutez-y le contenu suivant :
```bash
#!/bin/sh
cd $1
git gui 
```

(*) Changez les droits d'accès de ce fichier pour autoriser son exécution.

## Installation rapide

Copiez-collez cette ligne dans un terminal :
```
sudo apt-get install git git-gui nautilus-actions && sudo echo '#!/bin/sh' > /opt/git-gui-script.sh && sudo echo 'cd $1' >> /opt/git-gui-script.sh && sudo echo 'git gui' >> /opt/git-gui-script.sh && sudo chmod a+x /opt/git-gui-script.sh && nautilus-actions-config-tool
```

Dans la fenêtre qui s'ouvre ensuite, modifiez les informations comme indiqué ci-dessus.