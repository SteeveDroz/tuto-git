# Nouveau projet

La création d'un nouveau projet se fait en deux temps :
1. Création du projet sur GitLab.
2. Clone du projet sur l'ordinateur.

## Création du projet sur GitLab

La création d'un nouveau projet sur GitLab se fait en cliquant sur le bouton [New Project](https://gitlab.com/projects/new) du tableau de bord GitLab.

Il est standard de donner un nom au projet qui contient uniquement des minuscules et en spéarant les mots par des tirets. Exemple : `space-invader`.

Un projet peut être public, interne ou privé. Le tableau ci-dessous décrit les différents choix :

| Utilisateur        | Action | Private | Internal | Public |
| -----------------: | ------ | :-----: | :------: | :----: |
| Anonyme            | Voir   | Non     | Non      | Oui    |
|                    | Cloner | Non     | Non      | Oui    |
|                    | Éditer | Non     | Non      | Non    |
| Utilisateur GitLab | Voir   | Non°    | Oui      | Oui    |
|                    | Cloner | Non°    | Oui      | Oui    |
|                    | Éditer | Non°    | Non°     | Non°   |
| Vous               | Voir   | Oui     | Oui      | Oui    |
|                    | Cloner | Non°°   | Non°°    | Non°°  |
|                    | Éditer | Oui     | Oui      | Oui    |

° Sauf si vous lui donnez les droits d'accès.
°° Vous ne pouvez pas cloner votre propre projet.

## Clone du projet sur l'ordinateur

1. Sur votre ordinateur, faites un clic-droit sur le bureau ou dans un répertoire et choisissez " Git GUI ".
2. Choisissez " Clone existing repository " (ou " Cloner un dépot existant ").
3. Précisez l'adresse du dépôt : `https://[nom]@gitlab.com/[nom]/[depot].git`, `[nom]` étant votre nom d'utilisateur GitLab et `[depot]` étant le nom du dépôt que vous avez créé.
4. Précisez le chemin vers un nouveau répertoire où sera situé votre projet sur votre ordinateur.
5. Cliquez sur " Clone ".